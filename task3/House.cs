﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class House
    {
        public string Address { get; set; }
        public int Rooms { get; set; }

        public House(string address, int rooms)
        {
            Address = address;
            Rooms = rooms;
        }

        public House Clone()
        {
            return (House)MemberwiseClone();
        }

        public House DeepClone()
        {
            return new House(Address, Rooms);
        }

    }
}
