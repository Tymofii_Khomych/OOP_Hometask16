﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            House originalHouse = new House("123 Main St", 3);

            // Поверхневе копіювання
            House shallowCopy = originalHouse.Clone();
            Console.WriteLine("Shallow Copy:");
            Console.WriteLine("Address: " + shallowCopy.Address);
            Console.WriteLine("Rooms: " + shallowCopy.Rooms);
            Console.WriteLine();

            // Зміна значень у поверхневій копії
            shallowCopy.Address = "789 Oak St";
            shallowCopy.Rooms = 5;

            Console.WriteLine("Shallow Copy (після змін):");
            Console.WriteLine("Address: " + shallowCopy.Address);
            Console.WriteLine("Rooms: " + shallowCopy.Rooms);
            Console.WriteLine();

            // Зміни не впливають на оригінальний об'єкт
            Console.WriteLine("Original House (після змін у поверхневій копії):");
            Console.WriteLine("Address: " + originalHouse.Address);
            Console.WriteLine("Rooms: " + originalHouse.Rooms);
            Console.WriteLine();

            // Глибоке копіювання
            House deepCopy = originalHouse.DeepClone();
            Console.WriteLine("Deep Copy:");
            Console.WriteLine("Address: " + deepCopy.Address);
            Console.WriteLine("Rooms: " + deepCopy.Rooms);
            Console.WriteLine();

            // Зміна значень у глибокій копії
            deepCopy.Address = "456 Elm St";
            deepCopy.Rooms = 2;

            Console.WriteLine("Deep Copy (після змін):");
            Console.WriteLine("Address: " + deepCopy.Address);
            Console.WriteLine("Rooms: " + deepCopy.Rooms);
            Console.WriteLine();

            // Зміни не впливають на оригінальний об'єкт
            Console.WriteLine("Original House (після змін у глибокій копії):");
            Console.WriteLine("Address: " + originalHouse.Address);
            Console.WriteLine("Rooms: " + originalHouse.Rooms);
            Console.WriteLine();
        }
    }
}
