﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    internal struct Dot
    {
        int x;
        int y;
        int z;

        public Dot(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static Dot operator +(Dot dot1, Dot dot2)
        {
            return new Dot(dot1.x + dot2.x, dot1.y + dot2.y, dot1.z + dot2.z);
        }

        public override string ToString() 
        { 
            return "x = " + x + ", y = " + y + ", z = " + z;
        }
    }
}
