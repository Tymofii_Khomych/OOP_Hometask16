﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Dot dot1 = new Dot(2, 7, 4);
            Dot dot2 = new Dot(5, 2, -5);
            
            Dot result = dot1 + dot2;
            Console.WriteLine(result.ToString());
        }
    }
}
