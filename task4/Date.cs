﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Date
    {
        int day;
        int month;
        int year;

        public Date(int day, int month, int year)
        {
            this.day = day;
            this.month = month;
            this.year = year;
        }
        
        public static int operator -(Date date1, Date date2)
        {
            DateTime _date1 = new DateTime(date1.year, date1.month, date1.day);
            DateTime _date2 = new DateTime(date2.year, date2.month, date2.day);
            TimeSpan duration = _date1 - _date2;
            return duration.Days;
        }

        public static Date operator +(Date date, int addDays)
        {
            Date _date = new Date(date.day, date.month, date.year); 
            _date.day += addDays; 

            while (_date.day > DateTime.DaysInMonth(_date.year, _date.month)) 
            {
                _date.day -= DateTime.DaysInMonth(_date.year, _date.month); 
                _date.month++; 

                if (_date.month > 12) 
                {
                    _date.month = 1; 
                    _date.year++; 
                }
            }

            return _date;
        }

        public override string ToString()
        {
            return $"{day}.{month}.{year}";
        }
    }
}
