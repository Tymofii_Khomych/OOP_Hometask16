﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Date date1 = new Date(02, 4, 2020);
            Date date2 = new Date(08, 6, 2021);

            Console.WriteLine("First date = " + date1.ToString());
            Console.WriteLine("Second date = " + date2.ToString());

            Console.WriteLine(date2 - date1);

            int addDays = 50;
            Console.WriteLine(date1.ToString() + $" + {addDays} days = " + (date1 + addDays).ToString());
        }
    }
}
