﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Block block1 = new Block(1, 2, 3 ,4);
            Block block2 = new Block(5, 6, 7 ,8);
            Block block3 = new Block(5, 6, 7 ,8);

            Console.WriteLine(block1.Equals(block2));
            Console.WriteLine(block2.Equals(block3));
            Console.WriteLine(block1.ToString());
        }
    }
}
