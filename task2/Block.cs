﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Block
    {
        int a;
        int b;
        int c;
        int d;

        public Block(int a, int b, int c, int d)
        {
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
        }

        public override bool Equals(object obj)
        {
            if(obj == null ||  obj.GetType() != typeof(Block))
            {
                return false;
            }
            Block block = obj as Block;
            return (a == block.a) && (b == block.b) && (c == block.c) && (d == block.d);
        }

        public override string ToString()
        {
            return $"a = {a}\nb = {b}\nc = {c}\nd = {d}";
        }
    }
}
